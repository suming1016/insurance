/**
 * Custom icon list
 * All icons are loaded here for easy management
 * @see https://vue.ant.design/components/icon/#Custom-Font-Icon
 *
 * 自定义图标加载表
 * 所有图标均从这里加载，方便管理
 */

import bugIcon from '@/assets/icons/bug.svg?inline'
import buildIcon from '@/assets/icons/build.svg?inline'
import buttonIcon from '@/assets/icons/button.svg?inline'
import bxAnalyseIcon from '@/assets/icons/bxAnalyse.svg?inline'
import cascaderIcon from '@/assets/icons/cascader.svg?inline'
import chartIcon from '@/assets/icons/chart.svg?inline'
import checkboxIcon from '@/assets/icons/checkbox.svg?inline'
import clipboardIcon from '@/assets/icons/clipboard.svg?inline'
import codeIcon from '@/assets/icons/code.svg?inline'
import colorIcon from '@/assets/icons/color.svg?inline'
import componentIcon from '@/assets/icons/component.svg?inline'
import dashboardIcon from '@/assets/icons/dashboard.svg?inline'
import dateRangeIcon from '@/assets/icons/dateRange.svg?inline'
import dateIcon from '@/assets/icons/date.svg?inline'
import dictIcon from '@/assets/icons/dict.svg?inline'
import documentationIcon from '@/assets/icons/documentation.svg?inline'
import downloadIcon from '@/assets/icons/download.svg?inline'
import dragIcon from '@/assets/icons/drag.svg?inline'
import druidIcon from '@/assets/icons/druid.svg?inline'
import editIcon from '@/assets/icons/edit.svg?inline'
import eyeOpenIcon from '@/assets/icons/eyeOpen.svg?inline'
import githubIcon from '@/assets/icons/github.svg?inline'
import guideIcon from '@/assets/icons/guide.svg?inline'
import inputIcon from '@/assets/icons/input.svg?inline'
import jobIcon from '@/assets/icons/job.svg?inline'
import logIcon from '@/assets/icons/log.svg?inline'
import logininforIcon from '@/assets/icons/logininfor.svg?inline'
import messageIcon from '@/assets/icons/message.svg?inline'
import monitorIcon from '@/assets/icons/monitor.svg?inline'
import numberIcon from '@/assets/icons/number.svg?inline'
import onlineIcon from '@/assets/icons/online.svg?inline'
import passwordIcon from '@/assets/icons/password.svg?inline'
import pdfIcon from '@/assets/icons/pdf.svg?inline'
import peoplesIcon from '@/assets/icons/peoples.svg?inline'
import phoneIcon from '@/assets/icons/phone.svg?inline'
import postIcon from '@/assets/icons/post.svg?inline'
import questionIcon from '@/assets/icons/question.svg?inline'
import radioIcon from '@/assets/icons/radio.svg?inline'
import rateIcon from '@/assets/icons/rate.svg?inline'
import redisIcon from '@/assets/icons/redis.svg?inline'
import rowIcon from '@/assets/icons/row.svg?inline'
import selectIcon from '@/assets/icons/select.svg?inline'
import serverIcon from '@/assets/icons/server.svg?inline'
import swaggerIcon from '@/assets/icons/swagger.svg?inline'
import switchIcon from '@/assets/icons/switch.svg?inline'
import systemIcon from '@/assets/icons/system.svg?inline'
import textareaIcon from '@/assets/icons/textarea.svg?inline'
import timeRangeIcon from '@/assets/icons/timeRange.svg?inline'
import timeIcon from '@/assets/icons/time.svg?inline'
import toolIcon from '@/assets/icons/tool.svg?inline'
import treeTableIcon from '@/assets/icons/treeTable.svg?inline'
import treeIcon from '@/assets/icons/tree.svg?inline'
import uploadIcon from '@/assets/icons/upload.svg?inline'
import userIcon from '@/assets/icons/user.svg?inline'
import validCodeIcon from '@/assets/icons/validCode.svg?inline'


//新增
import educationIcon from '@/assets/icons/education.svg?inline'
import emailIcon from '@/assets/icons/email.svg?inline'
import exampleIcon from '@/assets/icons/example.svg?inline'
import excelIcon from '@/assets/icons/excel.svg?inline'
import exitFullscreenIcon from '@/assets/icons/exit-fullscreen.svg?inline'
import eyeIcon from '@/assets/icons/eye.svg?inline'
import formIcon from '@/assets/icons/form.svg?inline'
import fullscreenIcon from '@/assets/icons/fullscreen.svg?inline'
import iconIcon from '@/assets/icons/icon.svg?inline'
import internationalIcon from '@/assets/icons/international.svg?inline'
import languageIcon from '@/assets/icons/language.svg?inline'
import linkIcon from '@/assets/icons/link.svg?inline'
import listIcon from '@/assets/icons/list.svg?inline'
import lockIcon from '@/assets/icons/lock.svg?inline'
import moneyIcon from '@/assets/icons/money.svg?inline'
import nestedIcon from '@/assets/icons/nested.svg?inline'
import peopleIcon from '@/assets/icons/people.svg?inline'
import qqIcon from '@/assets/icons/qq.svg?inline'
import searchIcon from '@/assets/icons/search.svg?inline'
import shoppingIcon from '@/assets/icons/shopping.svg?inline'
import sizeIcon from '@/assets/icons/size.svg?inline'
import skillIcon from '@/assets/icons/skill.svg?inline'
import sliderIcon from '@/assets/icons/slider.svg?inline'
import starIcon from '@/assets/icons/star.svg?inline'
import tabIcon from '@/assets/icons/tab.svg?inline'
import themeIcon from '@/assets/icons/theme.svg?inline'
import wechatIcon from '@/assets/icons/wechat.svg?inline'
import zipIcon from '@/assets/icons/zip.svg?inline'



const allIcon = {
    bugIcon,
    buildIcon,
    buttonIcon,
    bxAnalyseIcon,
    cascaderIcon,
    chartIcon,
    checkboxIcon,
    clipboardIcon,
    codeIcon,
    colorIcon,
    componentIcon,
    dashboardIcon,
    dateRangeIcon,
    dateIcon,
    dictIcon,
    documentationIcon,
    downloadIcon,
    dragIcon,
    druidIcon,
    editIcon,
    eyeOpenIcon,
    githubIcon,
    guideIcon,
    inputIcon,
    jobIcon,
    logIcon,
    logininforIcon,
    messageIcon,
    monitorIcon,
    numberIcon,
    onlineIcon,
    passwordIcon,
    pdfIcon,
    peoplesIcon,
    phoneIcon,
    postIcon,
    questionIcon,
    radioIcon,
    rateIcon,
    redisIcon,
    rowIcon,
    selectIcon,
    serverIcon,
    swaggerIcon,
    switchIcon,
    systemIcon,
    textareaIcon,
    timeRangeIcon,
    timeIcon,
    toolIcon,
    treeTableIcon,
    treeIcon,
    uploadIcon,
    userIcon,
    validCodeIcon,
    //新增
    educationIcon,
    emailIcon,
    excelIcon,
    exampleIcon,
    exitFullscreenIcon,
    eyeIcon,
    formIcon,
    fullscreenIcon,
    iconIcon,
    internationalIcon,
    languageIcon,
    linkIcon,
    listIcon,
    lockIcon,
    moneyIcon,
    nestedIcon,
    peopleIcon,
    qqIcon,
    searchIcon,
    shoppingIcon,
    sizeIcon,
    skillIcon,
    sliderIcon,
    starIcon,
    tabIcon,
    themeIcon,
    wechatIcon,
    zipIcon
}

export default allIcon
