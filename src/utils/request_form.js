import Router from "@/router";
import store from "@/store";
import { ACCESS_TOKEN } from "@/store/mutation-types";
import errorCode from "@/utils/errorCode";
import { blobValidate } from "@/utils/ruoyi";
import message from "ant-design-vue/es/message";
import notification from "ant-design-vue/es/notification";
import { saveAs } from "file-saver";
import qs from "qs";
import storage from "store";
const baseUrl = process.env.VUE_APP_BASE_API;
let isReloginShow;
// 请求拦截器
const requestInterceptor = async (url, options) => {
  let urls = url;
  options.mode = "cors";
  // 在发送请求之前做些什么
  const token = storage.get(ACCESS_TOKEN);
  if (token) {
    options.headers = {
      Authorization: "Bearer " + token,
      // "Content-Type": "application/json",
      ...options.headers,
      "Content-Type": "application/json", // 设置 Content-Type 头
    };
  } else {
    options.headers = {
      ...options.headers,
      "Content-Type": "application/json", // 设置 Content-Type 头
    };
  }
  if (options.params) {
    let obj = options.params;
    Object.keys(obj).forEach((key) => {
      if (obj[key] == null || obj[key] === "") {
        delete obj[key];
      }
    });
    urls = url + "?" + qs.stringify(obj, { indices: false });
    options.params = {};
  }
  if (options.method == "post") {
    let daat = qs.parse(options.data);
    let obj = daat;
    Object.keys(obj).forEach((key) => {
      if (obj[key] == null || obj[key] === "") {
        delete obj[key];
      }
    });
    if (daat.pageNum || daat.pageSize) {
      urls =
        url +
        "?pageNum=" +
        (daat.pageNum ? daat.pageNum : 1) +
        "&pageSize=" +
        (daat.pageSize ? daat.pageSize : 10);
    }
    options.body = JSON.stringify(obj);
  }
  return {
    urls,
    options,
  };
};

// 响应拦截器
const responseInterceptor = async (res, req) => {
  // 请求rul
  const requestUrl = req.url;
  // 未设置状态码则默认成功状态
  const code = res.code || 200;
  // 获取错误信息
  const msg = errorCode[code] || res.msg || errorCode["default"];
  // 二进制数据则直接返回
  if (req.responseType === "blob" || req.responseType === "arraybuffer") {
    return res.data;
  }
  if (code === 401) {
    if (!isReloginShow) {
      isReloginShow = true;
      notification.open({
        message: "系统提示",
        description: "登录状态已过期，请重新登录",
        btn: (h) => {
          return h(
            "a-button",
            {
              props: {
                type: "primary",
                size: "small",
              },
              on: {
                click: () => {
                  store.dispatch("Logout").then(() => {
                    isReloginShow = false;
                    // location.href = location.href.split("#")[0];
                    Router.push({ path: "/user/login" });
                  });
                },
              },
            },
            "确认"
          );
        },
        duration: 0,
        onClose: () => {
          isReloginShow = false;
        },
      });
    }
  } else if (code === 500) {
    if (requestUrl !== "/login") {
      notification.error({
        message: "提示",
        description: msg,
      });
    } else {
      notification.error({
        message: "提示",
        description: msg,
      });
    }
  } else if (code !== 200) {
    notification.error({
      message: msg,
    });
  } else {
    return res;
  }
  return Promise.reject(msg);
};

// 发送请求的函数
const request = async (options = {}) => {
  const controller = new AbortController();
  const timeoutId = setTimeout(
    () => controller.abort(),
    options.timeout || 600000
  ); // 默认超时时间为 10 秒
  try {
    // 执行请求拦截器
    const { urls: interceptedUrl, options: interceptedOptions } =
      await requestInterceptor(baseUrl + options.url, options);
    // 发送请求
    let resdata = await fetch(interceptedUrl, {
      ...interceptedOptions,
      signal: controller.signal,
    })
      .then((resss) => {
        return resss.json();
      })
      .then((datas) => {
        return responseInterceptor(datas, options);
      });
    return Promise.resolve(resdata);
    // 执行响应拦截器
  } catch (error) {
    clearTimeout(timeoutId);
    let { message } = error;
    if (message === "Network Error") {
      message = "后端接口连接异常";
    } else if (message.includes("timeout")) {
      message = "系统接口请求超时";
    } else if (message.includes("Request failed with status code")) {
      message = "系统接口" + message.substr(message.length - 3) + "异常";
    }
    notification.error({
      message: message,
      duration: 2,
    });
    return Promise.reject(error);
  }
};
// 通用下载方法
export function download(url, params, filename) {
  const notificationKey = "download";
  notification.open({
    key: notificationKey,
    message: "正在下载数据，请稍候",
    duration: null,
    icon: (h) => {
      return h("a-icon", {
        props: {
          type: "loading",
        },
      });
    },
  });
  return request
    .post(url, params, {
      transformRequest: [
        (params) => {
          return qs.stringify(params);
        },
      ],
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      responseType: "blob",
    })
    .then(async (data) => {
      const isLogin = await blobValidate(data);
      if (isLogin) {
        const blob = new Blob([data]);
        saveAs(blob, filename);
        message.success("下载成功");
      } else {
        const resText = await data.text();
        const rspObj = JSON.parse(resText);
        const errMsg =
          errorCode[rspObj.code] || rspObj.msg || errorCode["default"];
        message.error(errMsg);
      }
      notification.close(notificationKey);
    })
    .catch((r) => {
      message.error("下载文件出现错误，请联系管理员！");
      notification.close(notificationKey);
    });
}

export default request;
