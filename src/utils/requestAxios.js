import Router from "@/router";
import store from "@/store";
import { ACCESS_TOKEN } from "@/store/mutation-types";
import errorCode from "@/utils/errorCode";
import { blobValidate } from "@/utils/ruoyi";
import message from "ant-design-vue/es/message";
import notification from "ant-design-vue/es/notification";
import axios from "axios";
import { saveAs } from "file-saver";
import qs from "qs";
import storage from "store";
import { VueAxios } from "./axios";
// 是否显示重新登录
let isReloginShow;

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_BASE_API,
  // baseURL: 'https://ruoyi.setworld.net',
  timeout: 600000, // 请求超时时间
});

// 异常拦截处理器
const errorHandler = (error) => {
  let { message } = error;
  if (message === "Network Error") {
    message = "后端接口连接异常";
  } else if (message.includes("timeout")) {
    message = "系统接口请求超时";
  } else if (message.includes("Request failed with status code")) {
    message = "系统接口" + message.substr(message.length - 3) + "异常";
  }
  notification.error({
    message: message,
    duration: 2,
  });
  return Promise.reject(error);
};

// request interceptor
request.interceptors.request.use((config) => {
  const token = storage.get(ACCESS_TOKEN);
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  if (token) {
    config.headers["Authorization"] = "Bearer " + token; // 让每个请求携带自定义token 请根据实际情况自行修改
    // config.headers['accessAccess-Token'] = token
  }
  // 处理params参数
  if (config.params) {
    let obj = config.params;
    Object.keys(obj).forEach((key) => {
      if (obj[key] == null || obj[key] === "") {
        delete obj[key];
      }
    });
    const url = config.url + "?" + qs.stringify(obj, { indices: false });
    config.params = {};
    config.url = url;
  }
  if (config.method == "post") {
    // console.log("store: ", store.state.user.csrfToken);
    let daat = qs.parse(config.data);
    if (daat.pageNum || daat.pageSize) {
      const url =
        config.url +
        "?pageNum=" +
        (daat.pageNum ? daat.pageNum : 1) +
        "&pageSize=" +
        (daat.pageSize ? daat.pageSize : 10);
      config.url = url;
      // if (daat.pageNum) {
      //   delete daat[pageNum];
      // }
      // if (daat.pageSize) {
      //   delete daat[pageSize];
      // }
      // config.data = qs.stringify(daat);
    }
  }

  // if (config.method === "post" || config.method === "put") {
  //   config.data = { ...config.data, time: new Date().getTime() };
  // }
  return config;
}, errorHandler);

// response interceptor
request.interceptors.response.use((res) => {
  // 请求rul
  const requestUrl = res.config.url;
  // 未设置状态码则默认成功状态
  const code = res.data.code || 200;
  // 获取错误信息
  const msg = errorCode[code] || res.data.msg || errorCode["default"];
  // 二进制数据则直接返回
  if (
    res.request.responseType === "blob" ||
    res.request.responseType === "arraybuffer"
  ) {
    return res.data;
  }
  if (code === 401) {
    if (!isReloginShow) {
      isReloginShow = true;
      notification.open({
        message: "系统提示",
        description: "登录状态已过期，请重新登录",
        btn: (h) => {
          return h(
            "a-button",
            {
              props: {
                type: "primary",
                size: "small",
              },
              on: {
                click: () => {
                  store.dispatch("Logout").then(() => {
                    isReloginShow = false;
                    // location.href = location.href.split("#")[0];
                    Router.push({ path: "/user/login" });
                  });
                },
              },
            },
            "确认"
          );
        },
        duration: 0,
        onClose: () => {
          isReloginShow = false;
        },
      });
    }
  } else if (code === 500) {
    if (requestUrl !== "/login") {
      notification.error({
        message: "提示",
        description: msg,
      });
    } else {
      notification.error({
        message: "提示",
        description: msg,
      });
    }
  } else if (code !== 200) {
    notification.error({
      message: msg,
    });
  } else {
    return res.data;
  }
  return Promise.reject(msg);
}, errorHandler);

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, request);
  },
};

// 通用下载方法
export function download(url, params, filename) {
  const notificationKey = "download";
  notification.open({
    key: notificationKey,
    message: "正在下载数据，请稍候",
    duration: null,
    icon: (h) => {
      return h("a-icon", {
        props: {
          type: "loading",
        },
      });
    },
  });
  return request
    .post(url, params, {
      transformRequest: [
        (params) => {
          return qs.stringify(params);
        },
      ],
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      responseType: "blob",
    })
    .then(async (data) => {
      const isLogin = await blobValidate(data);
      if (isLogin) {
        const blob = new Blob([data]);
        saveAs(blob, filename);
        message.success("下载成功");
      } else {
        const resText = await data.text();
        const rspObj = JSON.parse(resText);
        const errMsg =
          errorCode[rspObj.code] || rspObj.msg || errorCode["default"];
        message.error(errMsg);
      }
      notification.close(notificationKey);
    })
    .catch((r) => {
      message.error("下载文件出现错误，请联系管理员！");
      notification.close(notificationKey);
    });
}

export default request;

export { installer as VueAxios, request as axios };
