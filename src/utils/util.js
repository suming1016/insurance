import { isExternal } from "@/utils/validate";

export function timeFix() {
  const time = new Date();
  const hour = time.getHours();
  return hour < 9
    ? "早上好"
    : hour <= 11
    ? "上午好"
    : hour <= 13
    ? "中午好"
    : hour < 20
    ? "下午好"
    : "晚上好";
}

export function welcome() {
  const arr = [
    "休息一会儿吧",
    "准备吃什么呢?",
    "要不要打一把 DOTA",
    "我猜你可能累了",
  ];
  const index = Math.floor(Math.random() * arr.length);
  return arr[index];
}

/**
 * 触发 window.resize
 */
export function triggerWindowResizeEvent() {
  const event = document.createEvent("HTMLEvents");
  event.initEvent("resize", true, true);
  event.eventType = "message";
  window.dispatchEvent(event);
}

export function handleScrollHeader(callback) {
  let timer = 0;

  let beforeScrollTop = window.pageYOffset;
  callback = callback || function () {};
  window.addEventListener(
    "scroll",
    (event) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        let direction = "up";
        const afterScrollTop = window.pageYOffset;
        const delta = afterScrollTop - beforeScrollTop;
        if (delta === 0) {
          return false;
        }
        direction = delta > 0 ? "down" : "up";
        callback(direction);
        beforeScrollTop = afterScrollTop;
      }, 50);
    },
    false
  );
}

export function isIE() {
  const bw = window.navigator.userAgent;
  const compare = (s) => bw.indexOf(s) >= 0;
  const ie11 = (() => "ActiveXObject" in window)();
  return compare("MSIE") || ie11;
}

/**
 * Remove loading animate
 * @param id parent element id or class
 * @param timeout
 */
export function removeLoadingAnimate(id = "", timeout = 1500) {
  if (id === "") {
    return;
  }
  setTimeout(() => {
    document.body.removeChild(document.getElementById(id));
  }, timeout);
}

export async function showImagesInViewer(
  imagesUrl,
  watermarks,
  index,
  noWatermarks
) {
  let urls = imagesUrl.split(";");
  let arr = [];

  if (noWatermarks) {
    arr = urls.map((item) => {
      if (isExternal(item)) {
        return item;
      }
      return "https://airuibiao.oss-cn-chengdu.aliyuncs.com/" + item;
    });
  } else {
    for (let i = 0; i < urls.length; i++) {
      const res = await getCanvan(watermarks, urls[i]);
      arr.push(res);
    }
  }

  urls instanceof Array &&
    urls?.length &&
    this.$viewerApi({
      images: arr,
      options: {
        initialViewIndex: index,
      },
    });
}
export async function showImagesInViewer2(
  imagesUrl,
  watermarks,
  index,
  noWatermarks
) {
  let urls = imagesUrl.split(";");
  let arr = [];

  if (noWatermarks) {
    arr = urls.map((item) => {
      if (isExternal(item)) {
        return item;
      }
      return "https://airuibiao.oss-cn-chengdu.aliyuncs.com/" + item;
    });
  } else {
    for (let i = 0; i < urls.length; i++) {
      const res = await getCanvan2(watermarks, urls[i]);
      arr.push(res);
    }
  }

  urls instanceof Array &&
    urls?.length &&
    this.$viewerApi({
      images: arr,
      options: {
        initialViewIndex: index,
      },
    });
}
function getCanvan(watermarks, url) {
  return new Promise((resolve) => {
    let arr = url.split("/");
    let str1 = arr[arr.length - 1];
    if (!(str1.split("_")[0] == "001" && str1.indexOf("_007_") == -1)) {
      resolve(url);
      return;
    }

    var img = new Image();
    img.crossOrigin = "Anonymous";
    img.src = url;
    img.onload = () => {
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;

      let bit = 1;

      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);

      ctx.font = bit * 28 + "px Arial";
      ctx.fillStyle = "#f00";

      let watermarksList = watermarks.split(",");
      for (let i = 0; i < watermarksList.length; i++) {
        ctx.fillText(watermarksList[i], 32 * bit, 60 * bit + 30 * i);
      }

      let result = canvas.toDataURL("image/jpeg");

      resolve(result);
    };
  });
}

function getCanvan2(watermarks, url) {
  return new Promise((resolve) => {
    let arr = url.split("/");

    var img = new Image();
    img.crossOrigin = "Anonymous";
    img.src = url;

    img.onload = () => {
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;

      let bit = 1;

      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);

      ctx.font = bit * 28 + "px Arial";
      ctx.fillStyle = "#f00";

      let watermarksList = watermarks.split(",");
      for (let i = 0; i < watermarksList.length; i++) {
        ctx.fillText(watermarksList[i], 32 * bit, 60 * bit + 30 * i);
      }

      let result = canvas.toDataURL("image/jpeg");

      resolve(result);
    };
  });
}
