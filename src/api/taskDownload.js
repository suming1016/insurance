import request from '@/utils/request'



// 添加下载戴标图片任务
export function downloadTagImage(data) {
    return request({
        url: '/taskDownload/downloadTagImage',
        method: 'post',
        data: data
    })
}

// 添加下载养殖户图片任务
export function downloadFarmingImage(data) {
    return request({
        url: '/taskDownload/downloadFarmingImage',
        method: 'post',
        data: data
    })
}



// 添加下载戴标图片和养殖户
export function downloadTagAndFarmingImage(data) {
    return request({
        url: '/taskDownload/downloadTagAndFarmingImage',
        method: 'post',
        data: data
    })
}



// 修改任务状态
export function editStatus(data) {
    return request({
        url: '/taskDownload/editStatus',
        method: 'post',
        data: data
    })
}

// 下载任务查询
export function downloadTagImagePage(data) {
    return request({
        url: '/taskDownload/page',
        method: 'post',
        data: data
    })
}

