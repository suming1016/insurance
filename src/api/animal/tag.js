import request from "@/utils/request";

// 查询耳标列表
export function listInfo(query) {
  return request({
    url: "/tag/info/list",
    method: "post",
    data: query,
  });
}
//牲畜类型
export function animalTypeList(data) {
  return request({
    url: "/animal/animalType/list",
    method: "post",
    data,
  });
}
// 查询耳标详细
export function getInfos(id) {
  return request({
    url: "/tag/info/parmKey?parmKey=" + id,
    method: "post",
  });
}
//耳标清单
export function download(query) {
  return request({
    method: "post",
    url: "/tag/info/export",
    responseType: "blob",
    data: query,
  });
}

//承保清单
export function download1(query) {
  return request({
    method: "post",
    url: "/tag/info/export/accept/insurance",
    responseType: "blob",
    data: query,
  });
}

//分户清单
export function download2(query) {
  return request({
    method: "post",
    url: "/tag/info/export/farmer/inventory",
    responseType: "blob",
    data: query,
  });
}

//人保公示清单
export function download3(query) {
  return request({
    method: "post",
    url: "/tag/info/exportPublicityAll",
    responseType: "blob",
    data: query,
  });
}
//搜索养殖户
export function searchFaarming(query) {
  return request({
    url: "/farming/info/searchFaarming?keyword=" + query.keyword,
    method: "post",
    // data: query,
  });
}
// 查询图片审核列表
export function getInfo(query) {
  return request({
    url: "/tag/info/imageAudit",
    method: "post",
    data: query,
  });
}
// 图片审核确认
export function confirm(ids) {
  return request({
    url: "/tag/info/confirm/parmKey?parmKey=" + ids,
    method: "post",
  });
}
// 修改耳标(清单)
export function updateInfo(data) {
  return request({
    url: "/tag/info/edit",
    method: "post",
    data: data,
  });
}
