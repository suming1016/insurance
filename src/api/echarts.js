import request from "@/utils/request";

//戴标按天统计
export function statisDay(regionCode) {
  return request({
    url: "/tag/statis/day/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//戴标按月统计
export function statisMonth(regionCode) {
  return request({
    url: "/tag/statis/month/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//戴标按季度统计
export function statisQuarter(regionCode) {
  return request({
    url: "/tag/statis/quarter/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//戴标按年统计
export function statisYear(regionCode) {
  return request({
    url: "/tag/statis/year/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}

//戴标按区域统计
export function statisRegion(regionCode) {
  return request({
    url: "/tag/statis/region/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}

//戴标按大类统计
export function statisType(regionCode) {
  return request({
    url: "/tag/statis/type/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}

//承保按天统计
export function tagInsuranceStatisDay(regionCode) {
  return request({
    url: "/tagInsurance/statis/day/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//承保按月统计
export function tagInsuranceStatisMonth(regionCode) {
  return request({
    url: "/tagInsurance/statis/month/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//承保按季度统计
export function tagInsuranceStatisQuarter(regionCode) {
  return request({
    url: "/tagInsurance/statis/quarter/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//承保按年统计
export function tagInsuranceStatisYear(regionCode) {
  return request({
    url: "/tagInsurance/statis/year/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}
//承保按大类统计
export function tagInsuranceStatisType(regionCode) {
  return request({
    url: "/tagInsurance/statis/type/parmKey?parmKey=" + regionCode,
    method: "post",
  });
}

//理赔统计
export function dpWarnStatistics(data) {
  return request({
    url: "/dp/warnStatistics?parmKey=" + data.regionCode + "&type=" + data.type,
    method: "post",
    // data,
  });
}

//死亡原因统计
export function dpDieReasonStatistics(data) {
  return request({
    url: "/dp/dieReasonStatistics?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}

//牲畜死亡分析
export function dpAnimalDieAnalyse(data) {
  return request({
    url: "/dp/animalDieAnalyse?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}

//理赔大类统计
export function dpWarnType(data) {
  return request({
    url: "/dp/warnType?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}

//死损率预警
export function dieEarlyWarn(data) {
  return request({
    url: "/dp/dieEarlyWarn?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}

//续保预警统计
export function renewEarlyWarn(data) {
  return request({
    url: "/dp/renewEarlyWarn?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}

//高死损用户
export function dieEarlyWarnStatistics(data) {
  return request({
    url: "/dp/dieEarlyWarnStatistics?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}

//高死损用户
export function mortalityRateStatistics(data) {
  return request({
    url: "/dp/mortalityRateStatistics?parmKey=" + data.regionCode,
    method: "post",
    // data,
  });
}
