import request from "@/utils/request";
// 查询每日戴标统计列表
export function uploadStatisticsList(query) {
  return request({
    url: "/tagUpload/list",
    method: "post",
    data: query,
  });
}

export function adminSearch(query) {
  return request({
    url: "/adminSearch.htm?" + qs.stringify(params),
    method: "post",
  });
}
export function StatisticsDetailsFarmers(query) {
  return request({
    url: "/tagUpload/farming",
    method: "post",
    data: query,
  });
}
//上传统计耳标明细列表
export function StatisticsDetailsEarTag(query) {
  return request({
    url: "/tagUpload/tag",
    method: "post",
    data: query,
  });
}
// 根据关键字查询当前机构下戴标员
export function getUserByKeyword(data) {
  return request({
    url: "/system/user/getUserByKeyword/parmKey?parmKey=" + data,
    method: "post",
  });
}
//搜索养殖户
export function searchFaarming(query) {
  return request({
    url: "/farming/info/searchFaarming",
    method: "post",
    data: query,
  });
}
