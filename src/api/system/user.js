import request from "@/utils/request";
import { parseStrEmpty } from "@/utils/ruoyi";

// 查询用户列表
export function listUser(query) {
  return request({
    url: "/system/user/list",
    method: "post",
    data: query,
  });
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: "/system/user/parmKey?parmKey=" + parseStrEmpty(userId),
    method: "post",
  });
}

// 新增用户
export function addUser(data) {
  return request({
    url: "/system/user",
    method: "post",
    data: data,
  });
}

// 修改用户
export function updateUser(data) {
  return request({
    url: "/system/user/edit",
    method: "post",
    data: data,
  });
}

// 删除用户
export function delUser(userId) {
  return request({
    url: "/system/user/del/parmKey?parmKey=" + userId,
    method: "post",
  });
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password,
  };
  return request({
    url: "/system/user/resetPwd/edit",
    method: "post",
    data: data,
  });
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  const data = {
    userId,
    status,
  };
  return request({
    url: "/system/user/changeStatus/edit",
    method: "post",
    data,
  });
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: "/system/user/profile",
    method: "post",
  });
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: "/system/user/profile/edit",
    method: "post",
    data: data,
  });
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword,
  };
  return request({
    url: "/system/user/profile/updatePwd/edit",
    method: "post",
    params: data,
  });
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: "/system/user/profile/avatar",
    method: "post",
    data: data,
  });
}

// 导入用户
export function importData(data) {
  return request({
    url: "/system/user/importData",
    method: "post",
    data: data,
  });
}

// 查询授权角色
export function getAuthRole(userId) {
  return request({
    url: "/system/user/authRole/parmKey?parmKey=" + userId,
    method: "post",
  });
}

// 保存授权角色
export function updateAuthRole(data) {
  return request({
    url: "/system/user/authRole/edit",
    method: "post",
    params: data,
  });
}

// 查询机构下拉树结构
export function deptTreeSelect() {
  return request({
    url: "/system/dept/treeselect",
    method: "post",
  });
}
//用户数据权限查询
export function regionDeptRole(userId) {
  return request({
    url: "/system/user/regionDept/parmKey?parmKey=" + userId,
    method: "post",
  });
}
//用户数据权限修改
export function regionEditRole(data) {
  return request({
    url: "/system/user/regionDeptEdit/edit",
    method: "post",
    data: data,
  });
}
// 根据关键字查询
export function getUserByKeyword(data) {
  return request({
    url: "/system/user/getUserByKeyword/parmKey?parmKey=" + data,
    method: "post",
  });
}
