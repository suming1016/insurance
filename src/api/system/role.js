import request from "@/utils/request";

// 查询角色列表
export async function listRole(query) {
  const body = JSON.stringify(query);
  // 发送 POST 请求，并在 body 中传递参数
  let datas = await fetch(process.env.VUE_APP_BASE_API + "/system/role/list", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: body,
  }).then((response) => {
    return response.json();
  });
  return Promise.resolve(datas);
}

// 查询角色详细
export function getRole(roleId) {
  return request({
    url: "/system/role/parmKey?parmKey=" + roleId,
    method: "post",
  });
}

// 新增角色
export function addRole(data) {
  return request({
    url: "/system/role",
    method: "post",
    data: data,
  });
}

// 修改角色
export function updateRole(data) {
  return request({
    url: "/system/role/edit",
    method: "post",
    data: data,
  });
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: "/system/role/dataScope/edit",
    method: "post",
    data: data,
  });
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    roleId,
    status,
  };
  return request({
    url: "/system/role/changeStatus/edit",
    method: "post",
    data: data,
  });
}

// 删除角色
export function delRole(roleId) {
  return request({
    url: "/system/role/del/parmKey?parmKey=" + roleId,
    method: "post",
  });
}

// 查询角色已授权用户列表
export function allocatedUserList(query) {
  return request({
    url: "/system/role/authUser/allocatedList",
    method: "post",
    data: query,
  });
}

// 查询角色未授权用户列表
export function unallocatedUserList(query) {
  return request({
    url: "/system/role/authUser/unallocatedList",
    method: "post",
    data: query,
  });
}

// 取消用户授权角色
export function authUserCancel(data) {
  return request({
    url: "/system/role/authUser/cancel/edit",
    method: "post",
    data: data,
  });
}

// 批量取消用户授权角色
export function authUserCancelAll(data) {
  return request({
    url: "/system/role/authUser/cancelAll/edit",
    method: "post",
    params: data,
  });
}

// 授权用户选择
export function authUserSelectAll(data) {
  return request({
    url: "/system/role/authUser/selectAll/edit",
    method: "post",
    params: data,
  });
}

// 根据角色ID查询机构树结构
export function deptTreeSelect(roleId) {
  return request({
    url: "/system/dept/roleDeptTreeselect/parmKey?parmKey=" + roleId,
    method: "post",
  });
}

//根据角色id查询菜单列表

export function getMenus(id) {
  return request({
    url: "/system/role/getMenus/parmKey?parmKey=" + id,
    method: "post",
  });
}

// 配置角色权限
export function setPower(data) {
  return request({
    url: "/system/role/power",
    method: "post",
    data: data,
  });
}

// 根据机构id获取角色列表
export function getRolesByDeptId(data) {
  return request({
    url: "/system/role/getRolesByDeptId",
    method: "post",
    data: data,
  });
}

// 角色数据权限查询
export function regionDeptRole(data) {
  return request({
    url: "/system/role/regionDept/parmKey?parmKey=" + data,
    method: "post",
  });
}

// 角色数据权限修改
export function regionEditRole(data) {
  return request({
    url: "/system/role/regionDeptEdit/edit",
    method: "post",
    data: data,
  });
}
