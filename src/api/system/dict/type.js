import request from "@/utils/request";

// 查询字典类型列表
export function listType(query) {
  return request({
    url: "/system/dict/type/list",
    method: "post",
    data: query,
  });
}

// 查询字典类型详细
export function getType(dictId) {
  return request({
    url: "/system/dict/type/parmKey?parmKey=" + dictId,
    method: "post",
  });
}

// 新增字典类型
export function addType(data) {
  return request({
    url: "/system/dict/type",
    method: "post",
    data: data,
  });
}

// 修改字典类型
export function updateType(data) {
  return request({
    url: "/system/dict/type/edit",
    method: "post",
    data: data,
  });
}

// 删除字典类型
export function delType(dictId) {
  return request({
    url: "/system/dict/type/del/parmKey?parmKey=" + dictId,
    method: "post",
  });
}

// 刷新参数缓存
export function refreshCache() {
  return request({
    url: "/system/dict/type/refreshCache/del/",
    method: "post",
  });
}

// 获取字典选择框列表
export function optionselect() {
  return request({
    url: "/system/dict/type/optionselect",
    method: "post",
  });
}
