import request from "@/utils/request";

// 查询机构列表
export function listDept(query) {
  return request({
    url: "/system/dept/list",
    method: "post",
    data: query,
  });
}

// 查询机构列表（排除节点）
export function listDeptExcludeChild(deptId) {
  return request({
    url: "/system/dept/list/exclude/parmKey?parmKey=" + deptId,
    method: "post",
  });
}

// 查询机构详细
export function getDept(deptId) {
  return request({
    url: "/system/dept/parmKey?parmKey=" + deptId,
    method: "post",
  });
}

// 新增机构
export function addDept(data) {
  return request({
    url: "/system/dept",
    method: "post",
    data: data,
  });
}

// 修改机构
export function updateDept(data) {
  return request({
    url: "/system/dept/edit",
    method: "post",
    data: data,
  });
}

// 删除机构
export function delDept(deptId) {
  return request({
    url: "/system/dept/del/parmKey?parmKey=" + deptId,
    method: "post",
  });
}
// 查询机构
export function treeselect() {
  return request({
    url: "/system/dept/userDeptList",
    method: "post",
  });
}

// 查询机构(权限)
export function treeselectNew() {
  return request({
    url: "/system/dept/deptDataList",
    method: "post",
  });
}

// 用户数据权限查询
export function regionDept(userId) {
  return request({
    url: "/system/user/regionDept/parmKey?parmKey=" + userId,
    method: "post",
  });
}

// 用户数据权限修改
export function regionDeptEdit(data) {
  return request({
    url: "/system/user/regionDeptEdit/edit",
    method: "post",
    data: data,
  });
}

// /system/dept/queryDeptByKey/
export function queryDeptByKey(query) {
  return request({
    url: "/system/dept/queryDeptByKey",
    method: "post",
    data: query,
  });
}

// 机构配置获取
export function deptConfigGet() {
  return request({
    url: "/system/deptConfig/get",
    method: "post",
  });
}
