import request from "@/utils/request";

export function exportList(data) {
  return request({
    url: "/mortality/rate/list",
    method: "post",
    data: data,
  });
}
