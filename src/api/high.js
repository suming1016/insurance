import request from "@/utils/request";

// 修改高风险
export function systemHighEdit(data) {
  return request({
    url: "/system/high/edit",
    method: "post",
    data: data,
  });
}

// 新增高风险
export function systemHighAdd(data) {
  return request({
    url: "/system/high",
    method: "post",
    data: data,
  });
}

// 删除高风险
export function systemHighDel(ids) {
  return request({
    url: "/system/high/del/parmKey?parmKey=" + ids,
    method: "post",
  });
}

// 查询高风险
export function systemHighList(data) {
  return request({
    url: "/system/high/list",
    method: "post",
    data: data,
  });
}
