// import { encryptAES } from "@/utils/aes";
import request from "@/utils/request";

//查询区域
export function getParentCode(parentCode) {
  return request({
    url: "/sys/region/getRegionByParentCode/parmKey?parmKey=" + parentCode,
    method: "post",
  });
}
//查询区域详细信息
export function getCode(code) {
  return request({
    url: "/sys/region/parmKey?parmKey=" + code,
    method: "post",
  });
}

//省级
export function getParentCodeFirst() {
  return request({
    url: "/sys/region/getRegionByParentCode/parmKey?parmKey=",
    method: "post",
  });
}

//过滤区域
export function getRegionHigherList(code) {
  let codes = code; // encryptAES(code);
  return request({
    url: "/sys/region/getRegionHigherList/parmKey?parmKey=" + codes,
    method: "post",
  });
}
