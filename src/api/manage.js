import request from "@/utils/request";

const api = {
  user: "/user",
  role: "/role",
  service: "/service",
  permission: "/permission",
  permissionNoPager: "/permission/no-pager",
  orgTree: "/org/tree",
};

export default api;

export function getUserList(parameter) {
  return request({
    url: api.user,
    method: "post",
    data: parameter,
  });
}

export function getRoleList(parameter) {
  return request({
    url: api.role,
    method: "post",
    data: parameter,
  });
}

export function getServiceList(parameter) {
  return request({
    url: api.service,
    method: "post",
    data: parameter,
  });
}

export function getPermissions(parameter) {
  return request({
    url: api.permissionNoPager,
    method: "post",
    data: parameter,
  });
}

export function getOrgTree(parameter) {
  return request({
    url: api.orgTree,
    method: "post",
    data: parameter,
  });
}

// id == 0 add     post
// id != 0 update  put
export function saveService(parameter) {
  return request({
    url: api.service,
    method: parameter.id === 0 ? "post" : "put",
    data: parameter,
  });
}

export function saveSub(sub) {
  return request({
    url: "/sub",
    method: sub.id === 0 ? "post" : "put",
    data: sub,
  });
}
