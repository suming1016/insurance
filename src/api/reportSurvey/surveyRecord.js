import request from "@/utils/request";

// 查询查勘列表数据
export function surveyV3List(query) {
  return request({
    url: "/surveyV3/list",
    method: "post",
    data: query,
  });
}
// 查询查勘详情数据
export function getSurveyDetail(query) {
  return request({
    url: "/surveyV3/getSurveyDetail?parmKey=" + query.id,
    method: "post",
    // data: query,
  });
}
