import request from "@/utils/request";

// 查询报案列表数据
export function warnInfoList(query) {
  return request({
    url: "/warn/info/list",
    method: "post",
    data: query,
  });
}
// 查询报案详情
export function getWarnDetail(query) {
  return request({
    url: "/warn/info/getWarnDetail?parmKey=" + query.id,
    method: "post",
    // data: query,
  });
}
// 撤案
export function cancelWarn(query) {
  return request({
    url: "/warn/info/cancelWarn",
    method: "post",
    data: query,
  });
}
