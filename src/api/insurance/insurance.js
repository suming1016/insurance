import request from "@/utils/request";

// 查询列表
export function listTagInsurance(query) {
  return request({
    url: "/tag/insurance/list",
    method: "post",
    data: query,
  });
}
// 详情
export function detailsTagInsurance(id) {
  return request({
    url: "/tag/insurance/parmKey?parmKey=" + id,
    method: "post",
  });
}

// 新增
export function addTagInsurance(data) {
  return request({
    url: "/tag/insurance",
    method: "post",
    data: data,
  });
}
// 删除
export function delTagInsurance(id) {
  return request({
    url: "/tag/insurance/del/parmKey?parmKey=" + id,
    method: "post",
  });
}
//选择养殖户
export function farmingInsuranceList(query) {
  return request({
    url: "/tag/insurance/tagFarmingList",
    method: "post",
    data: query,
  });
}
//选择耳标
export function tagNoFarmingList(query) {
  return request({
    url: "/tag/insurance/getTagsByFarmingId",
    method: "post",
    data: query,
  });
}
