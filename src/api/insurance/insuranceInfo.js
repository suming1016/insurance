import request from "@/utils/request";

// 查询列表
export function listTagInsuranceInfo(query) {
  return request({
    url: "/tag/insuranceInfo/list",
    method: "post",
    data: query,
  });
}
// 详情
export function detailsTagInsuranceInfo(id) {
  return request({
    url: "/tag/insuranceInfo/parmKey?parmKey=" + id,
    method: "post",
  });
}

// 导出
export function download(query) {
  return request({
    url: "/tag/insuranceInfo/export",
    method: "post",
    responseType: "blob",
    data: query,
  });
}

// 详情
export function downloadTemp() {
  return request({
    url: "/tag/insuranceInfo/downloadTemplate",
    method: "post",
    responseType: "blob",
  });
}

//导入
export function importInfo(data) {
  return request({
    url: "/tag/insuranceInfo/importInfo",
    method: "post",
    data: data,
  });
}
export function importInfo2bd(data) {
  return request({
    url: "/tag/insuranceInfo/gzrb/importInfo",
    method: "post",
    data: data,
  });
}
//导入
export function importTagHis(data) {
  return request({
    url: "/tag/info/import/tagHis",
    method: "post",
    data: data,
  });
}
// 导出
export function downloadTagHis(query) {
  return request({
    url: "/tag/info/export/tagHis",
    method: "post",
    data: query,
    responseType: "blob",
  });
}
