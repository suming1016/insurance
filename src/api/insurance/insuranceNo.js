import request from "@/utils/request";

// 查询列表
export function listTagInsuranceNo(query) {
  return request({
    url: "/tag/insuranceNo/list",
    method: "post",
    data: query,
  });
}
// 详情
export function detailsTagInsuranceNo(id) {
  return request({
    url: "/tag/insuranceNo/parmKey?parmKey=" + id,
    method: "post",
  });
}
// 分页表格
export function detailsTagInsuranceNoTags(id) {
  return request({
    url: "/tag/insuranceNo/tags/parmKey?parmKey=" + id,
    method: "post",
  });
}
