import request from '@/utils/request'

// 上传
export function uploadFile(formData) {
    return request({
        url: '/file/upload',
        method: 'post',
        data: formData,
        headers: {
            'Content-type': 'multipart/form-data',
        },
    })
}