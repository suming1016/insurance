import request from "@/utils/request";
// 人口档案列表
export function listInfo(query) {
  return request({
    url: "/population/info/list",
    method: "post",
    data: query,
  });
}
//查看
export function getInfo(id) {
  return request({
    url: "/population/info/parmKey?parmKey=" + id,
    method: "post",
  });
}
//导入
export function imports(data) {
  return request({
    url: "/population/info/import",
    method: "POST",
    headers: {
      "Content-type": "multipart/form-data",
    },
    data: data,
  });
}
export function downloadStateTemplate(params) {
  return request({
    method: "post",
    url: "/population/info/export.htm",
    responseType: "blob",
    data: params,
  });
}
