import request from "@/utils/request";

// 查询列表
export function listInsurance(query) {
  return request({
    url: "/animal/insurance/list",
    method: "post",
    data: query,
  });
}
// 详情
export function detailsInsurance(id) {
  return request({
    url: "/animal/insurance/parmKey?parmKey=" + id,
    method: "post",
  });
}

// 新增
export function addInsurance(data) {
  return request({
    url: "/animal/insurance",
    method: "post",
    data: data,
  });
}

// 修改
export function upInsurance(data) {
  return request({
    url: "/animal/insurance/edit",
    method: "post",
    data: data,
  });
}

// 删除
export function delInsurance(id) {
  return request({
    url: "/animal/insurance/del/parmKey?parmKey=" + id,
    method: "post",
  });
}

export function deptConfigPut(data) {
  return request({
    url: "/system/deptConfig/edit",
    method: "post",
    data: data,
  });
}
// 机构配置查询
export function deptConfig(deptId) {
  return request({
    url: "/system/deptConfig/get/",
    method: "post",
  });
}
