import request from "@/utils/request";

// 查询操作日志列表
export function list(query) {
  let guid = "";
  for (let i = 1; i <= 32; i++) {
    const n = Math.floor(Math.random() * 16.0).toString(16);
    guid += n;
  }
  return request({
    url: "/monitor/operlog/list",
    method: "post",
    data: { ...query, userToken: "Bearer" + guid },
  });
}

// 删除操作日志
export function delOperlog(operId) {
  return request({
    url: "/monitor/operlog/del/parmKey?parmKey=" + operId,
    method: "post",
  });
}

// 清空操作日志
export function cleanOperlog() {
  return request({
    url: "/monitor/operlog/clean/del",
    method: "post",
  });
}
