import request from "@/utils/request";

// 查询在线用户列表
export function list(query) {
  return request({
    url: "/monitor/online/list",
    method: "post",
    data: query,
  });
}

// 强退用户
export function forceLogout(tokenId) {
  return request({
    url: "/monitor/online/del/parmKey?parmKey=" + tokenId,
    method: "post",
  });
}
