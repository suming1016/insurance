import request from "@/utils/request";

// 查询缓存详细
export function getCache() {
  return request({
    url: "/monitor/cache",
    method: "post",
  });
}

// 查询缓存名称列表
export function listCacheName() {
  return request({
    url: "/monitor/cache/getNames",
    method: "post",
  });
}

// 查询缓存键名列表
export function listCacheKey(cacheName) {
  return request({
    url: "/monitor/cache/getKeys/parmKey?parmKey=" + cacheName,
    method: "post",
  });
}

// 查询缓存内容
// export function getCacheValue(cacheName, cacheKey) {
//   return request({
//     url: "/monitor/cache/getValue/" + cacheName + "/" + cacheKey,
//     method: "get",
//   });
// }

// 清理指定名称缓存
export function clearCacheName(cacheName) {
  return request({
    url: "/monitor/cache/clearCacheName/del/parmKey?parmKey=" + cacheName,
    method: "post",
  });
}

// 清理指定键名缓存
export function clearCacheKey(cacheKey) {
  return request({
    url: "/monitor/cache/clearCacheKey/del/parmKey?parmKey=" + cacheKey,
    method: "post",
  });
}

// 清理全部缓存
export function clearCacheAll() {
  return request({
    url: "/monitor/cache/clearCacheAll/del",
    method: "post",
  });
}
