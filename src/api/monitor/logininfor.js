import request from "@/utils/request";

// 查询登录日志列表
export function list(query) {
  return request({
    url: "/monitor/logininfor/list",
    method: "post",
    data: query,
  });
}

// 删除登录日志
export function delLogininfor(infoId) {
  return request({
    url: "/monitor/logininfor/del/parmKey?parmKey=" + infoId,
    method: "post",
  });
}

// 清空登录日志
export function cleanLogininfor() {
  return request({
    url: "/monitor/logininfor/clean/del",
    method: "post",
  });
}

// 解锁用户登录状态
export function unlockLogininfor(userName) {
  return request({
    url: "/monitor/logininfor/unlock/parmKey?parmKey=" + userName,
    method: "post",
  });
}
