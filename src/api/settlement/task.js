import request from "@/utils/request";

// 查询报案列表
export function listWarn(query) {
  return request({
    url: "/warn/info/list",
    method: "post",
    data: query,
  });
}
// 报案详情
export function detailsWarn(id) {
  return request({
    url: "/warn/info/getWarnDetail/parmKey?parmKey=" + id,
    method: "post",
  });
}
//撤案
export function cancelWarn(data) {
  return request({
    url: "/warn/info/cancelWarn",
    method: "post",
    data: data,
  });
}
