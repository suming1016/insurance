import request from "@/utils/request";

// 查询理赔列表
export function listSurvey(query) {
  return request({
    url: "/surveyV2/list",
    method: "post",
    data: query,
  });
}

// 修改
export function EditSurvey(data) {
  return request({
    url: "/surveyV2/edit",
    method: "post",
    data: data,
  });
}

// 查勘详情
export function detailsSurvey(id) {
  return request({
    url: "/surveyV2/parmKey?parmKey=" + id,
    method: "post",
  });
}

// 查勘导出
export function download(query) {
  return request({
    url: "/surveyV2/export",
    method: "post",
    responseType: "blob",
    data: query,
  });
}

// 信息对比
export function surveyV2Contrast(id) {
  return request({
    url: "/surveyV2/contrast/parmKey?parmKey=" + id,
    method: "post",
  });
}
