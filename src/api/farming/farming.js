import request from "@/utils/request";

// 查询养殖户列表
export function listInfo(query) {
  return request({
    url: "/farming/info/list",
    method: "post",
    data: query,
  });
}
// 查询养殖户详细
export function getInfo(id) {
  return request({
    url: "/farming/info/parmKey?parmKey=" + id,
    method: "post",
  });
}
//养殖户清单
export function download(query) {
  return request({
    method: "post",
    url: "/farming/info/exportFarmingAll",
    responseType: "blob",
    data: query,
  });
}
