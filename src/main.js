// with polyfills
import "core-js/stable";
import "regenerator-runtime/runtime";

import { TableSetting } from "@/components";
import FileUpload from "@/components/FileUpload";
import FooterToolBar from "@/components/FooterToolbar";
import ProLayout, { PageHeaderWrapper } from "@/components/ProLayout";
import dayjs from "dayjs";
import Vue from "vue";
import themePluginConfig from "../config/themePluginConfig";
import App from "./App.vue";
import permission from "./directive/permission";
import i18n from "./locales";
import router from "./router";
import store from "./store/";
import { VueAxios } from "./utils/requestAxios";

import ImagePreviewNew from "@/components/ImagePreviewNew";
import ImagePreviewNew2 from "@/components/ImagePreviewNew2";
import VueDraggableResizable from "vue-draggable-resizable";

// 字典数据组件
import DictData from "@/components/DictData";
// 字典标签组件
import { getConfigKey } from "@/api/system/config";
import { getDicts } from "@/api/system/dict/data";
import DictTag from "@/components/DictTag";
import { download } from "@/utils/requestAxios";
import {
  addDateRange,
  handleTree,
  parseTime,
  resetForm,
  selectDictLabel,
  selectDictLabels,
  tableSorter,
} from "@/utils/ruoyi";
import { showImagesInViewer, showImagesInViewer2 } from "@/utils/util";
import * as echarts from "echarts";
import moment from "moment"; //时间
import bootstrap from "./core/bootstrap";
import "./core/lazy_use"; // use lazy load components
import "./global.less"; // global style
import "./permission"; // permission control
import "./utils/filter"; // global filter
// GIS
// import '../public/IP_config'
// import '../public/cesium_package/Cesium.js'
// import '../public/cesium_package/cesium_bundle.js'
// import '../public/cesium_package/Widgets/widgets.css'

import "@/assets/reset.less";

import Viewer from "v-viewer";
import "viewerjs/dist/viewer.css";
Vue.use(Viewer);
Vue.prototype.showImagesInViewer = showImagesInViewer;
Vue.prototype.showImagesInViewer2 = showImagesInViewer2;

// 全局方法挂载
import axios from "axios";
import BaiduMap from "vue-baidu-map";
Vue.prototype.moment = moment;
Vue.prototype.$axios = axios;
Vue.prototype.getDicts = getDicts;
Vue.prototype.getConfigKey = getConfigKey;
Vue.prototype.parseTime = parseTime;
Vue.prototype.resetForm = resetForm;
Vue.prototype.addDateRange = addDateRange;
Vue.prototype.selectDictLabel = selectDictLabel;
Vue.prototype.selectDictLabels = selectDictLabels;
Vue.prototype.download = download;
Vue.prototype.handleTree = handleTree;
Vue.prototype.tableSorter = tableSorter;
Vue.config.productionTip = false;
Vue.prototype.$echarts = echarts;
Vue.use(BaiduMap, {
  ak: "Y525VfNhI1SPnpHGzr7snTSYzPx4Bkny",
});
Vue.prototype.dayjs = dayjs;
// mount axios to `Vue.$http` and `this.$http`
Vue.use(VueAxios);
// use pro-layout components
Vue.component("pro-layout", ProLayout);
Vue.component("page-container", PageHeaderWrapper);
Vue.component("page-header-wrapper", PageHeaderWrapper);
Vue.component("footer-tool-bar", FooterToolBar);
Vue.component("file-upload", FileUpload);
Vue.component("table-setting", TableSetting);
Vue.component("dict-tag", DictTag);
Vue.component("ImagePreviewNew", ImagePreviewNew);
Vue.component("ImagePreviewNew2", ImagePreviewNew2);
Vue.component("vue-draggable-resizable", VueDraggableResizable);

Vue.use(permission);
DictData.install();

window.umi_plugin_ant_themeVar = themePluginConfig.theme;
new Vue({
  router,
  store,
  i18n,
  // init localstorage, vuex
  created: bootstrap,
  render: (h) => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this;
  },
}).$mount("#app");
