import './index.less'

import debounce from 'lodash/debounce'
import PropTypes from 'ant-design-vue/es/_util/vue-types'
import { triggerEvent, inBrowser, isFun } from '../../utils/util'
import 'ant-design-vue/es/icon/style'
import Icon from 'ant-design-vue/es/icon'
import { defaultRenderLogo } from '../SiderMenu/SiderMenu'
import PageHeader, { PageHeaderProps } from 'ant-design-vue/es/page-header'


export const GlobalHeaderProps = {
  collapsed: PropTypes.bool,
  handleCollapse: PropTypes.func,
  isMobile: PropTypes.bool.def(false),
  fixedHeader: PropTypes.bool.def(false),
  logo: PropTypes.any,
  menuRender: PropTypes.any,
  collapsedButtonRender: PropTypes.any,
  headerContentRender: PropTypes.any,
  rightContentRender: PropTypes.any
}

const defaultRenderCollapsedButton = (h, collapsed) => (
  <Icon type={collapsed ? 'menu-unfold' : 'menu-fold'}/>
)

const renderPageHeader = (h, content, extraContent) => {
  if (!content && !extraContent) {
    return null
  }
  return (
    <div class={`${prefixedClassName}-detail`}>
      <div class={`${prefixedClassName}-main`}>
        <div class={`${prefixedClassName}-row`}>
          {content && (
            <div class={`${prefixedClassName}-content`}>{content}</div>
          )}
          {extraContent && (
            <div class={`${prefixedClassName}-extraContent`}>
              {extraContent}
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

const GlobalHeader = {
  name: 'GlobalHeader',
  props: GlobalHeaderProps,
  render (h) {
    const { isMobile, logo, rightContentRender, headerContentRender } = this.$props
    
    const { $route, $listeners } = this

    let breadcrumb = {}
    let routes = $route.matched.concat().filter(route => route && route.meta && route.meta.title)
    routes = routes.map(route => {
      if (route && route.meta && route.meta.title) {
        return {
          path: route.path,
          breadcrumbName: route.meta.title,
          redirect: route.redirect
        }
      }
    })
    const defaultItemRender = ({ route, params, routes, paths, h }) => {
      return (route.redirect === 'noRedirect' || routes.indexOf(route) === routes.length - 1) && (
        <span>{route.breadcrumbName}</span>
      ) || (
        <router-link to={{ path: route.path || '/', params }}>{route.breadcrumbName}</router-link>
      )
    }

    // If custom breadcrumb render undefined
    // use default breadcrumb..
    const itemRender = this.breadcrumbRender || defaultItemRender

    breadcrumb = { props: { routes, itemRender } }



    const toggle = () => {
      const { collapsed, handleCollapse } = this.$props
      if (handleCollapse) handleCollapse(!collapsed)
      this.triggerResizeEvent()
    }
    const renderCollapsedButton = () => {
      const {
        collapsed,
        collapsedButtonRender = defaultRenderCollapsedButton,
        menuRender
      } = this.$props




      const tabProps = {
        breadcrumb,
      }

      if (collapsedButtonRender !== false && menuRender !== false) {
        return (
          <div  class="my-global-head-wrap">
            <span class="ant-pro-global-header-trigger" onClick={toggle}>
            {isFun(collapsedButtonRender) && collapsedButtonRender(h, collapsed) || collapsedButtonRender}
           </span>
             <PageHeader {...{ props: tabProps }} >
               {renderPageHeader(h)}
              </PageHeader>
          </div>
        )
      }
      return null
    }

    const headerCls = 'ant-pro-global-header'

    return (
      <div class={headerCls}>
        {isMobile && (
          <a class={`${headerCls}-logo`} key="logo" href={'/'}>
            {defaultRenderLogo(h, logo)}
          </a>
        )}
        {renderCollapsedButton()}
        {headerContentRender && (
          <div class={`${headerCls}-content`}>
            {isFun(headerContentRender) && headerContentRender(h, this.$props) || headerContentRender}
          </div>
        )}
        {isFun(rightContentRender) && rightContentRender(h, this.$props) || rightContentRender}
      </div>
    )
  },
  methods: {
    triggerResizeEvent: debounce(() => {
      inBrowser && triggerEvent(window, 'resize')
    })
  },
  beforeDestroy () {
    this.triggerResizeEvent.cancel && this.triggerResizeEvent.cancel()
  }
}

export default GlobalHeader
