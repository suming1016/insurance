import { animalTypeList } from "@/api/animal/tag.js";
import { GetCsrfToken, getInfo, login, logout } from "@/api/login";
import { getParentCodeFirst } from "@/api/region.js";
import { deptConfigGet } from "@/api/system/dept";
import { ACCESS_TOKEN } from "@/store/mutation-types";
import storage from "store";

const user = {
  state: {
    token: "",
    name: "",
    welcome: "",
    avatar: "",
    deptId: null,
    roles: [],
    info: {},
    csrfToken: "",
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_DEPTID: (state, deptId) => {
      state.deptId = deptId;
    },
    SET_NAME: (state, name) => {
      state.name = name;
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar;
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_INFO: (state, info) => {
      state.info = info;
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions;
    },
    SET_CSRF_TOKEN: (state, CsrfToken) => {
      state.csrfToken = CsrfToken;
    },
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo)
          .then((res) => {
            storage.set(ACCESS_TOKEN, res.token, 7 * 24 * 60 * 60 * 1000);
            commit("SET_TOKEN", res.token);
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    GetCsrfToken({ commit, state }) {
      return new Promise((resolve, reject) => {
        GetCsrfToken(state.token)
          .then(async (res) => {
            console.log("res: ", res);
            commit("SET_CSRF_TOKEN", res);
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token)
          .then(async (res) => {
            const user = res.user;
            const avatar =
              user.avatar === ""
                ? require("@/assets/images/profile.jpg")
                : process.env.VUE_APP_BASE_API + user.avatar;
            if (res.roles && res.roles.length > 0) {
              // 验证返回的roles是否是一个非空数组
              commit("SET_ROLES", res.roles);
              commit("SET_PERMISSIONS", res.permissions);
            } else {
              commit("SET_ROLES", ["ROLE_DEFAULT"]);
            }
            commit("SET_DEPTID", user.deptId);
            commit("SET_NAME", user.nickName);
            commit("SET_AVATAR", avatar);
            const { rows } = await animalTypeList({
              pageSize: 100,
            });
            localStorage.setItem("animalTypeList", JSON.stringify(rows));

            const res1 = await getParentCodeFirst();
            localStorage.setItem("regionList", JSON.stringify(res1.data));

            const { data: data1 } = await deptConfigGet();
            commit("echarts/SET_REAL", data1);

            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    // 登出
    Logout({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token)
          .then(() => {
            commit("SET_TOKEN", "");
            commit("SET_ROLES", []);
            commit("SET_PERMISSIONS", []);
            storage.remove(ACCESS_TOKEN);
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};

export default user;
