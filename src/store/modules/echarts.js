const state = {
  regionCode: 3203,
  realConfig: {
    "systemName": "",
    "bigDataName": "",
    "regionCode": "",
    "mapType": "",
  }
}
const mutations = {
  SET_REGION(state, val) {
    state.regionCode = val
  },
  SET_REAL(state, val) {
    state.realConfig = val
  }
}

const actions = {
  setRegion({ commit }, data) {
    commit('SET_REGION', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
